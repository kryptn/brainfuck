#!/usr/bin/python

class Brainfuck(object):
   data = [0]
   code = ''
   position = 0
   pointer = 0

   commands = list('><+-.,[]')
   names = ['inc_ptr','dec_ptr','inc_data','dec_data','out','in','start','end']

   character = dict(zip(names, commands))

   def current_data(self):
      return self.data[self.pointer]
   
   def current_command(self):
      return self.code[self.position]

   def _mod_ptr(self, value):
      self.pointer += value   

   def _mod_data(self, value):
      try:
         self.data[self.pointer] += value
      except IndexError:
         self.data += [0 for x in xrange(self.pointer - len(self.data) + 1)]
         self.data[self.pointer] += value

   def _loop(self, value):
      if value == 1:
         if not self.current_data():
            self.position = self.code.find(self.character['end'], self.position)
      elif value == -1:
         self.position = self.code.rfind(self.character['start'], 0, self.position) - 1 

   def _output(self, t=None):
      print chr(self.current_data()),

   def _input(self, t=None):
      self.data[self.pointer] = ord(raw_input(':: >'))

   def validate(self):
      valid = 0
      for c in self.code:
         if c == '[':
            valid += 1
         elif c == ']':
            valid -= 1
      if valid:
         return False
      else:
         return True

   def tick(self):
      if self.current_command() in self.commands:
         com, arg = self.f[self.current_command()]
         com(arg)
         self.position += 1

   def run(self):
      while self.position < len(self.code):
         self.tick()

   def __init__(self, code):
      self.code = code
      self.data = [0]
      self.pointer = 0
      self.position = 0

      key = [(self._mod_ptr, 1),
             (self._mod_ptr, -1),
             (self._mod_data, 1),
             (self._mod_data, -1),
             (self._output, None),
             (self._input, None),
             (self._loop, 1),
             (self._loop, -1)]

      self.f = dict(zip(self.commands, key))

hw = '>+++++++++[<++++++++>-]<.>+++++++[<++++>-]<+.+++++++..+++.>>>++++++++[<++++>-]<.>>>++++++++++[<+++++++++>-]<---.<<<<.+++.------.--------.>>+.'
b = Brainfuck(hw)
